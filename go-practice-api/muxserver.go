package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)
func newRouter() *mux.Router {
	r := mux.NewRouter()
	fmt.Println("Got request from user!!")
	r.HandleFunc("/hello", func (w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	}).Methods("GET")
	return r
}

func main() {
	r := newRouter()
	log.Fatal(http.ListenAndServe(":8080", r))
}
func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World!")
}